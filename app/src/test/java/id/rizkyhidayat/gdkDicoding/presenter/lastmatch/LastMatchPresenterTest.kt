package id.rizkyhidayat.gdkDicoding.presenter.lastmatch

import id.rizkyhidayat.gdkDicoding.TestContextProvider
import id.rizkyhidayat.gdkDicoding.services.ApiServices
import id.rizkyhidayat.gdkDicoding.utils.InternetChecker
import id.rizkyhidayat.gdkDicoding.utils.Network
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class LastMatchPresenterTest {

    @Mock
    private lateinit var view: LastMatchContract.View

    private lateinit var network: Network

    private lateinit var presenter: LastMatchPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        network = Network()
        presenter = LastMatchPresenter(network, view, TestContextProvider())
    }

    @Test
    fun testGetLastMatch() {
        GlobalScope.launch {
            InternetChecker(object : InternetChecker.Consumer {
                override fun accept(internet: Boolean) {
                    if (internet) {
                        network.getInstance().create(ApiServices::class.java)
                                .eventPastLeague()
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({
                                    Mockito.verify(view).showEventList(it.events)
                                }, {
                                    error(it)
                                })
                    } else {
                        Mockito.verify(view).showSnackBar("You don't connect with internet")
                    }
                }
            })
        }
    }
}