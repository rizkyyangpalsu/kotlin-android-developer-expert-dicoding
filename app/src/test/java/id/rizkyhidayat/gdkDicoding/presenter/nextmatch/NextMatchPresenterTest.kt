package id.rizkyhidayat.gdkDicoding.presenter.nextmatch

import id.rizkyhidayat.gdkDicoding.TestContextProvider
import id.rizkyhidayat.gdkDicoding.services.ApiServices
import id.rizkyhidayat.gdkDicoding.utils.InternetChecker
import id.rizkyhidayat.gdkDicoding.utils.Network
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class NextMatchPresenterTest {

    @Mock
    private lateinit var view: NextMatchContracts.View

    private lateinit var network: Network

    private lateinit var presenter: NextMatchPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        network = Network()
        presenter = NextMatchPresenter(network, view, TestContextProvider())
    }

    @Test
    fun testGetNextMatch() {
        GlobalScope.launch {
            InternetChecker(object : InternetChecker.Consumer {
                override fun accept(internet: Boolean) {
                    if (internet) {
                        network.getInstance()
                                .create(ApiServices::class.java)
                                .eventNextLeague()
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({
                                    Mockito.verify(view).showEventList(it.events)
                                }, {
                                    error(it)
                                })
                    } else {
                        Mockito.verify(view).showSnackBar("You don't connect with internet")
                    }
                }
            })
        }
    }
}