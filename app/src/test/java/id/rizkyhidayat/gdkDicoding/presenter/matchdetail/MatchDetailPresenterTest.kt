package id.rizkyhidayat.gdkDicoding.presenter.matchdetail

import id.rizkyhidayat.gdkDicoding.TestContextProvider
import id.rizkyhidayat.gdkDicoding.services.ApiServices
import id.rizkyhidayat.gdkDicoding.utils.InternetChecker
import id.rizkyhidayat.gdkDicoding.utils.Network
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class MatchDetailPresenterTest {

    @Mock
    private lateinit var view: MatchDetailContracts.View

    private lateinit var network: Network

    private lateinit var presenter: MatchDetailPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        network = Network()
        presenter = MatchDetailPresenter(network, view, TestContextProvider())
    }

    @Test
    fun testGetMatchDetail() {
        val id = "576588"

        GlobalScope.launch {
            Mockito.verify(view).showLoading()
            InternetChecker(object : InternetChecker.Consumer {
                override fun accept(internet: Boolean) {
                    if (internet) {
                        Mockito.`when`(network.getInstance()
                                .create(ApiServices::
                                class.java)
                                .eventDetail(id)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(
                                        {
                                            Mockito.verify(view).hideLoading()
                                            Mockito.verify(view).showMatchDetail(it.events)
                                        },
                                        {
                                            error(it)
                                        }))
                    } else {
                        Mockito.verify(view).hideLoading()
                        Mockito.verify(view).showSnackBar("You don't connect with internet")
                    }
                }
            })
        }
    }

    @Test
    fun testGetHomeTeamDetail() {
        val id = "133610"

        GlobalScope.launch {
            Mockito.verify(view).showLoading()
            InternetChecker(object : InternetChecker.Consumer {
                override fun accept(internet: Boolean) {
                    if (internet) {
                        network.getInstance().create(ApiServices::class.java)
                                .teamDetail(id)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({
                                    Mockito.verify(view).hideLoading()
                                    Mockito.verify(view).showHomeTeamDetail(it.teams)
                                }, {
                                    error(it)
                                })
                    } else {
                        Mockito.verify(view).hideLoading()
                        Mockito.verify(view).showSnackBar("You don't connect with internet")
                    }
                }
            })
        }
    }

    @Test
    fun testGetAwayDetail() {
        val id = "133615"

        GlobalScope.launch {
            Mockito.verify(view).showLoading()
            InternetChecker(object : InternetChecker.Consumer {
                override fun accept(internet: Boolean) {
                    if (internet) {
                        network.getInstance().create(ApiServices::class.java)
                                .teamDetail(id)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({
                                    Mockito.verify(view).hideLoading()
                                    Mockito.verify(view).showAwayTeamDetail(it.teams)
                                }, {
                                    error(it)
                                })
                    } else {
                        Mockito.verify(view).hideLoading()
                        Mockito.verify(view).showSnackBar("You don't connect with internet")
                    }
                }
            })
        }
    }
}