package id.rizkyhidayat.gdkDicoding.views

import android.os.SystemClock
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.Espresso.pressBack
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.v7.widget.RecyclerView
import id.rizkyhidayat.gdkDicoding.R.id.*
import org.junit.Rule
import org.junit.Test

class MainActivityTest {

    @Rule
    @JvmField
    var activityRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun testAppBehaviour() {
        testLastMatchBehaviour()

        onView(withId(bottomNavigation))
                .check(matches(isDisplayed()))
        onView(withId(nextMatch))
                .perform(click())

        testNextMatchBehaviour()

        onView(withId(bottomNavigation))
                .check(matches(isDisplayed()))
        onView(withId(favorite))
                .perform(click())

        testFavoriteBehaviour()
    }

    private fun testLastMatchBehaviour() {
        SystemClock.sleep(2000)
        onView(withId(listLastMatch))
                .check(matches(isDisplayed()))
        onView(withId(listLastMatch))
                .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(10))
        onView(withId(listLastMatch))
                .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(10, click()))
        SystemClock.sleep(2000)
        onView(withId(addToFavorite))
                .check(matches(isDisplayed()))
        onView(withId(addToFavorite))
                .perform(click())
        pressBack()
    }

    private fun testNextMatchBehaviour() {
        SystemClock.sleep(2000)
        onView(withId(listNextMatch))
                .check(matches(isDisplayed()))
        onView(withId(listNextMatch))
                .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(12))
        onView(withId(listNextMatch))
                .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(12, click()))
        SystemClock.sleep(2000)
        onView(withId(addToFavorite))
                .check(matches(isDisplayed()))
        onView(withId(addToFavorite))
                .perform(click())
        pressBack()
    }

    private fun testFavoriteBehaviour() {
        onView(withId(listFavorite))
                .check(matches(isDisplayed()))
        onView(withId(listFavorite))
                .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(1))
        onView(withId(listFavorite))
                .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))
        SystemClock.sleep(2000)
        onView(withId(addToFavorite))
                .check(matches(isDisplayed()))
        onView(withId(addToFavorite))
                .perform(click())
        pressBack()
    }

}