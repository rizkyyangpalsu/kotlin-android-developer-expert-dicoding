package id.rizkyhidayat.gdkDicoding.utils

import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import id.rizkyhidayat.gdkDicoding.BuildConfig
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * @author <M.Rizky Hidayat, emrizkyha@gmail.com>
 */
class Network {

    private fun getOkHttpClient(): OkHttpClient {
        val client = OkHttpClient().newBuilder()
        val interceptor = Interceptor { chain ->
            val request = chain.request()?.newBuilder()
                    ?.build()
            val response = chain.proceed(request)

            if (response.code() == 403) {
                println("Nework error")
            }

            response
        }

        client.networkInterceptors().add(interceptor)
        return client.build()
    }

    fun getJsonBuilder(): Gson {
        return GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .setLenient()
                .create()
    }

    fun getInstance(): Retrofit {
        return Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(this.getOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create(this.getJsonBuilder()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }
}

