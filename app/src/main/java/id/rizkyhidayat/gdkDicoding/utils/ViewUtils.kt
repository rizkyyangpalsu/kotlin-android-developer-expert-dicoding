package id.rizkyhidayat.gdkDicoding.utils

import android.support.design.widget.Snackbar
import android.view.View

/**
 * @author <M.Rizky Hidayat, emrizkyha@gmail.com>
 */
fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.showSnackbar(message: String, duration: Int) {
    Snackbar.make(this, message, duration).show()
}