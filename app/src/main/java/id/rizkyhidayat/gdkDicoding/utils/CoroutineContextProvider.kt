package id.rizkyhidayat.gdkDicoding.utils

import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext

/**
 * @author <M.Rizky Hidayat, emrizkyha@gmail.com>
 */
open class CoroutineContextProvider {
    open val main: CoroutineContext by lazy { Dispatchers.Main }
}