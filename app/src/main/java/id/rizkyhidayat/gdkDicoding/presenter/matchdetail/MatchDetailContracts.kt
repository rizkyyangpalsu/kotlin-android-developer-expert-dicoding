package id.rizkyhidayat.gdkDicoding.presenter.matchdetail

import id.rizkyhidayat.gdkDicoding.models.EventDetail
import id.rizkyhidayat.gdkDicoding.models.TeamDetail
import id.rizkyhidayat.gdkDicoding.presenter.BasePresenter
import id.rizkyhidayat.gdkDicoding.presenter.BaseView

/**
 * @author <M.Rizky Hidayat, emrizkyha@gmail.com>
 */
interface MatchDetailContracts {

    interface View : BaseView<Presenter> {
        fun showMatchDetail(data: List<EventDetail>)
        fun showHomeTeamDetail(data: List<TeamDetail>)
        fun showAwayTeamDetail(data: List<TeamDetail>)
        fun showSnackBar(message: String)
        fun showLoading()
        fun hideLoading()
    }

    interface Presenter : BasePresenter<View> {
        fun getMatchDetail(idMatch: String)
        fun getHomeTeamDetail(idTeam: String)
        fun getAwayTeamDetail(idTeam: String)
    }

}