package id.rizkyhidayat.gdkDicoding.presenter.nextmatch

import id.rizkyhidayat.gdkDicoding.models.Event
import id.rizkyhidayat.gdkDicoding.presenter.BasePresenter
import id.rizkyhidayat.gdkDicoding.presenter.BaseView

/**
 * @author <M.Rizky Hidayat, emrizkyha@gmail.com>
 */
interface NextMatchContracts {

    interface View : BaseView<Presenter> {
        fun showEventList(data: List<Event>)
        fun showLoading()
        fun hideLoading()
        fun showSnackBar(message: String)
    }

    interface Presenter : BasePresenter<View> {

        fun getNextMatch()

    }

}