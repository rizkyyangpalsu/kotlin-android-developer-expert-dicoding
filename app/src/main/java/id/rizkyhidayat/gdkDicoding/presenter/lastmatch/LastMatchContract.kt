package id.rizkyhidayat.gdkDicoding.presenter.lastmatch

import id.rizkyhidayat.gdkDicoding.models.Event
import id.rizkyhidayat.gdkDicoding.presenter.BasePresenter
import id.rizkyhidayat.gdkDicoding.presenter.BaseView

/**
 * @author <M.Rizky Hidayat, emrizkyha@gmail.com>
 */
interface LastMatchContract {

    interface View : BaseView<Presenter> {
        fun showEventList(data: List<Event>)
        fun hideLoading()
        fun showLoading()
        fun showSnackBar(message: String)
    }

    interface Presenter : BasePresenter<View> {

        fun getLastMatch()

    }

}