package id.rizkyhidayat.gdkDicoding.presenter

/**
 * @author <M.Rizky Hidayat, emrizkyha@gmail.com>
 */
interface BasePresenter<T> {

    fun start()

    var view: T

}