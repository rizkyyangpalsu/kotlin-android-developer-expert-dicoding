package id.rizkyhidayat.gdkDicoding.services

import id.rizkyhidayat.gdkDicoding.models.responses.EventDetailResponse
import id.rizkyhidayat.gdkDicoding.models.responses.EventResponse
import id.rizkyhidayat.gdkDicoding.models.responses.TeamDetailResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * @author <M.Rizky Hidayat, emrizkyha@gmail.com>
 */
interface ApiServices {

    @GET("/api/v1/json/1/eventspastleague.php?id=4328")
    fun eventPastLeague(): Observable<EventResponse>

    @GET("/api/v1/json/1/eventsnextleague.php?id=4328")
    fun eventNextLeague(): Observable<EventResponse>

    @GET("/api/v1/json/1/lookupevent.php")
    fun eventDetail(
            @Query("id") idMatch: String
    ): Observable<EventDetailResponse>

    @GET("/api/v1/json/1/lookupteam.php")
    fun teamDetail(
            @Query("id") idTeam: String
    ): Observable<TeamDetailResponse>

}