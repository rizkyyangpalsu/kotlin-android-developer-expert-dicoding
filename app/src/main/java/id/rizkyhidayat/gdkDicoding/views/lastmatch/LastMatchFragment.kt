package id.rizkyhidayat.gdkDicoding.views.lastmatch

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.rizkyhidayat.gdkDicoding.R
import id.rizkyhidayat.gdkDicoding.models.Event
import id.rizkyhidayat.gdkDicoding.presenter.lastmatch.LastMatchContract
import id.rizkyhidayat.gdkDicoding.presenter.lastmatch.LastMatchPresenter
import id.rizkyhidayat.gdkDicoding.utils.Network
import id.rizkyhidayat.gdkDicoding.utils.invisible
import id.rizkyhidayat.gdkDicoding.utils.showSnackbar
import id.rizkyhidayat.gdkDicoding.utils.visible
import id.rizkyhidayat.gdkDicoding.views.matchdetail.MatchDetailActivity
import kotlinx.android.synthetic.main.fragment_last_match.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.support.v4.onRefresh

/**
 * @author <M.Rizky Hidayat, emrizkyha@gmail.com>
 */

class LastMatchFragment : Fragment(), LastMatchContract.View {

    private var listEvents: MutableList<Event> = mutableListOf()
    override lateinit var presenter: LastMatchContract.Presenter
    private lateinit var adapter: LastMatchAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        presenter = LastMatchPresenter(Network(), this)
        presenter.getLastMatch()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_last_match, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = LastMatchAdapter(listEvents) {
            context?.startActivity<MatchDetailActivity>(MatchDetailActivity.PARCEL to it)
        }
        listLastMatch.layoutManager = LinearLayoutManager(context)
        listLastMatch.adapter = adapter

        swipeRefreshListLastMatch.onRefresh {
            presenter.getLastMatch()
            swipeRefreshListLastMatch.isRefreshing = false
        }
    }

    override fun showEventList(data: List<Event>) {
        listEvents.clear()
        listEvents.addAll(data)
        adapter.notifyDataSetChanged()
    }

    override fun showSnackBar(message: String) {
        view?.showSnackbar(message, Snackbar.LENGTH_SHORT)
    }

    override fun hideLoading() {
        proggressBarLastMatch.invisible()
    }

    override fun showLoading() {
        proggressBarLastMatch.visible()
    }

}
