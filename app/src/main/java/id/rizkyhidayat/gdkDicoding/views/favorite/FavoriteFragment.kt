package id.rizkyhidayat.gdkDicoding.views.favorite

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.rizkyhidayat.gdkDicoding.R
import id.rizkyhidayat.gdkDicoding.database.database
import id.rizkyhidayat.gdkDicoding.models.Event
import id.rizkyhidayat.gdkDicoding.models.sqlite.Favorite
import id.rizkyhidayat.gdkDicoding.utils.invisible
import id.rizkyhidayat.gdkDicoding.utils.visible
import id.rizkyhidayat.gdkDicoding.views.matchdetail.MatchDetailActivity
import kotlinx.android.synthetic.main.fragment_favorite.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.support.v4.onRefresh

/**
 * @author <M.Rizky Hidayat, emrizkyha@gmail.com>
 */
class FavoriteFragment : Fragment() {

    private var favorites: MutableList<Event> = mutableListOf()
    private lateinit var adapter: FavoriteAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_favorite, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        showLoading()
        adapter = FavoriteAdapter(favorites) {
            context?.startActivity<MatchDetailActivity>(MatchDetailActivity.PARCEL to it)
        }

        listFavorite.layoutManager = LinearLayoutManager(context)
        listFavorite.adapter = adapter

        showFavorites()
        hideLoading()

        swiperRefreshFavorite.onRefresh {
            showLoading()
            favorites.clear()
            showFavorites()
            hideLoading()
        }
    }

    private fun showFavorites() {
        context?.database?.use {
            swiperRefreshFavorite.isRefreshing = false
            val result = select(Favorite.TABLE_NAME)
            val favorite = result.parseList(classParser<Event>())
            favorites.addAll(favorite)
            adapter.notifyDataSetChanged()
        }
    }

    private fun showLoading() {
        proggressBarFavorite.visible()
    }

    private fun hideLoading() {
        proggressBarFavorite.invisible()
    }

}