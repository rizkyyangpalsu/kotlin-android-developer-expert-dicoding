package id.rizkyhidayat.gdkDicoding.views.nextmatch

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.rizkyhidayat.gdkDicoding.R
import id.rizkyhidayat.gdkDicoding.models.Event
import id.rizkyhidayat.gdkDicoding.presenter.nextmatch.NextMatchContracts
import id.rizkyhidayat.gdkDicoding.presenter.nextmatch.NextMatchPresenter
import id.rizkyhidayat.gdkDicoding.utils.Network
import id.rizkyhidayat.gdkDicoding.utils.invisible
import id.rizkyhidayat.gdkDicoding.utils.showSnackbar
import id.rizkyhidayat.gdkDicoding.utils.visible
import id.rizkyhidayat.gdkDicoding.views.matchdetail.MatchDetailActivity
import kotlinx.android.synthetic.main.fragment_next_match.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.support.v4.onRefresh

/**
 * @author <M.Rizky Hidayat, emrizkyha@gmail.com>
 */
class NextMatchFragment : Fragment(), NextMatchContracts.View {

    override lateinit var presenter: NextMatchContracts.Presenter
    private lateinit var adapter: NextMatchAdapter
    private var listEvents: MutableList<Event> = mutableListOf()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        presenter = NextMatchPresenter(Network(), this)
        presenter.getNextMatch()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_next_match, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = NextMatchAdapter(listEvents) {
            context?.startActivity<MatchDetailActivity>(MatchDetailActivity.PARCEL to it)
        }
        listNextMatch.layoutManager = LinearLayoutManager(context)
        listNextMatch.adapter = adapter

        swipeRefreshListNextMatch.onRefresh {
            presenter.getNextMatch()
            swipeRefreshListNextMatch.isRefreshing = false
        }
    }

    override fun showEventList(data: List<Event>) {
        listEvents.clear()
        listEvents.addAll(data)
        adapter.notifyDataSetChanged()
    }

    override fun showSnackBar(message: String) {
        view?.showSnackbar(message, Snackbar.LENGTH_SHORT)
    }

    override fun showLoading() {
        proggressBarNextMatch.visible()
    }

    override fun hideLoading() {
        proggressBarNextMatch.invisible()
    }

}