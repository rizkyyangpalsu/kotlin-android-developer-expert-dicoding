package id.rizkyhidayat.gdkDicoding.views.nextmatch

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.rizkyhidayat.gdkDicoding.R
import id.rizkyhidayat.gdkDicoding.models.Event
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_list_last_match.*

/**
 * @author <M.Rizky Hidayat, emrizkyha@gmail.com>
 */
class NextMatchAdapter(
        private val items: List<Event>,
        private val listener: (Event) -> Unit
) : RecyclerView.Adapter<NextMatchAdapter.NextMatchHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NextMatchHolder {
        return NextMatchHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.item_list_next_match, parent, false)
        )
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: NextMatchHolder, position: Int) {
        holder.bindItem(items[position], listener)
    }

    inner class NextMatchHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
            LayoutContainer {

        @SuppressLint("NewApi")
        fun bindItem(item: Event, listener: (Event) -> Unit) {
            dateMatch.text = item.dateEvent
            homeMatch.text = item.strHomeTeam
            awayMatch.text = item.strAwayTeam
            homeScoreMatch.text = item.strHomeScore ?: " "
            awayScoreMatch.text = item.strAwayScore ?: " "

            containerView.setOnClickListener {
                listener(item)
            }
        }
    }
}
