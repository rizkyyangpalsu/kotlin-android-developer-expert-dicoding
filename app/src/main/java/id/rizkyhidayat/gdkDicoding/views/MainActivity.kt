package id.rizkyhidayat.gdkDicoding.views

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import id.rizkyhidayat.gdkDicoding.R
import id.rizkyhidayat.gdkDicoding.R.id.*
import id.rizkyhidayat.gdkDicoding.views.favorite.FavoriteFragment
import id.rizkyhidayat.gdkDicoding.views.lastmatch.LastMatchFragment
import id.rizkyhidayat.gdkDicoding.views.nextmatch.NextMatchFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                lastMatch -> {
                    loadLastMatchFragment(savedInstanceState)
                }
                nextMatch -> {
                    loadNextMatchFragment(savedInstanceState)
                }
                favorite -> {
                    loadFavoriteFragment(savedInstanceState)
                }
            }
            true
        }
        bottomNavigation.selectedItemId = lastMatch
    }

    private fun loadLastMatchFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.container, LastMatchFragment(), LastMatchFragment::class.simpleName)
                    .commit()
        }
    }

    private fun loadNextMatchFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.container, NextMatchFragment(), NextMatchFragment::class.simpleName)
                    .commit()
        }
    }

    private fun loadFavoriteFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.container, FavoriteFragment(), FavoriteFragment::class.simpleName)
                    .commit()
        }
    }
}
