package id.rizkyhidayat.gdkDicoding.models.responses

import id.rizkyhidayat.gdkDicoding.models.EventDetail

/**
 * @author <M.Rizky Hidayat, emrizkyha@gmail.com>
 */
data class EventDetailResponse(
        var events: List<EventDetail>
)