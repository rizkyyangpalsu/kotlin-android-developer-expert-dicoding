package id.rizkyhidayat.gdkDicoding.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * @author <M.Rizky Hidayat, emrizkyha@gmail.com>
 */
@Parcelize
data class TeamDetail(

        @SerializedName("idTeam")
        var idTeam: String?,

        @SerializedName("strTeam")
        var strTeam: String?,

        @SerializedName("strTeamBadge")
        var strTeamBadge: String?,

        @SerializedName("strDescriptionEN")
        var strDescriptionEN: String

) : Parcelable