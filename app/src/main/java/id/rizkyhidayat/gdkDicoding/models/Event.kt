package id.rizkyhidayat.gdkDicoding.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * @author <M.Rizky Hidayat, emrizkyha@gmail.com>
 */
@Parcelize
data class Event(

        var id: Long? = null,

        @SerializedName("idEvent")
        var idEvent: String? = null,

        @SerializedName("idHomeTeam")
        var idHomeTeam: String? = null,

        @SerializedName("idAwayTeam")
        var idAwayTeam: String? = null,

        @SerializedName("strHomeTeam")
        var strHomeTeam: String? = null,

        @SerializedName("strAwayTeam")
        var strAwayTeam: String? = null,

        @SerializedName("intHomeScore")
        var strHomeScore: String? = null,

        @SerializedName("intAwayScore")
        var strAwayScore: String? = null,

        @SerializedName("dateEvent")
        var dateEvent: String? = null

) : Parcelable