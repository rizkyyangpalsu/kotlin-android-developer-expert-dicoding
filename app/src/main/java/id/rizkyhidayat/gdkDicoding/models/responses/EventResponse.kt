package id.rizkyhidayat.gdkDicoding.models.responses

import id.rizkyhidayat.gdkDicoding.models.Event

/**
 * @author <M.Rizky Hidayat, emrizkyha@gmail.com>
 */
data class EventResponse(
        val events: List<Event>
)