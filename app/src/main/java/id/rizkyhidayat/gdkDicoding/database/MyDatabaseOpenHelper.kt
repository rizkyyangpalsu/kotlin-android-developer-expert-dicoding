package id.rizkyhidayat.gdkDicoding.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import id.rizkyhidayat.gdkDicoding.models.sqlite.Favorite
import org.jetbrains.anko.db.*

/**
 * @author <M.Rizky Hidayat, emrizkyha@gmail.com>
 */
class MyDatabaseOpenHelper(
        context: Context
) : ManagedSQLiteOpenHelper(context, "FavoriteMatch.db", null, 1) {

    companion object {
        private var instance: MyDatabaseOpenHelper? = null

        @Synchronized
        fun getInstance(context: Context): MyDatabaseOpenHelper {
            if (instance == null) {
                instance = MyDatabaseOpenHelper(context.applicationContext)
            }

            return instance as MyDatabaseOpenHelper
        }
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.createTable(Favorite.TABLE_NAME, true,
                Favorite.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                Favorite.EVENT_ID to TEXT + UNIQUE,
                Favorite.HOME_TEAM_ID to TEXT + UNIQUE,
                Favorite.AWAY_TEAM_ID to TEXT + UNIQUE,
                Favorite.HOME_TEAM_NAME to TEXT,
                Favorite.AWAY_TEAM_NAME to TEXT,
                Favorite.HOME_TEAM_SCORE to TEXT,
                Favorite.AWAY_TEAM_SCORE to TEXT,
                Favorite.DATE_EVENT to TEXT)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.dropTable(Favorite.TABLE_NAME, true)
    }
}

val Context.database: MyDatabaseOpenHelper
    get() = MyDatabaseOpenHelper.getInstance(applicationContext)
